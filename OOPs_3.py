# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 09:06:53 2018

@author: lili
"""
'''
 Python program to show that the variables with a value  
 assigned in class declaration, are class variables and 
 variables inside methods and constructors are instance 
 variables.
 
 '''
 
class Student:
    
    stream  = 'Electronics and Communication Engineering'
     
    def __init__(self,roll):
         
         
         #instance variable
        self.roll = roll


a = Student(1001)
print (a.stream)