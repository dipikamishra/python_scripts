# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 09:27:14 2018

@author: lili
"""


'''
 This will give an excpetion as we tried to access a hidden variable outside class
 '''
 
 
 
'''
We can access hidden variable through a tricky syntax
 
# write a python program to demonstrate that hidden members can be accessed outside a class
 
 '''
class Myclass:
     
     
     #Hidden member of myclass
     __hiddenmember = 10
     
     
#Drivercode
     
MyObject = Myclass()
print ("The hidden member value is: ",MyObject._Myclass__hiddenmember)