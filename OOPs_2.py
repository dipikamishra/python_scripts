# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 08:57:52 2018

@author: lili
"""

class Person:
    
    def __init__(self,name):
        self.name = name
    
    def say_Hi(self):
        print ("hello! My name is : ",self.name)

obj  = Person('Dipika')
obj.say_Hi()
    