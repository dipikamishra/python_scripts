# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 09:48:11 2018

@author: lili
"""

'''
Printing objects gives us information about objects we are working with
Like a friend function in C++
we can achieve this in python by using __str__ or __repr__

'''

class Test:
    def __init__(self,a,b):
        self.a = a
        self.b = b
        
    def __repr__(self):
        return ("Test a:%s,b:%s" % (self.a,self.b)  )
    
    
    def __str__(self):
        return ("the values of a and b are %s,%s: " %(self.a,self.b))


t = Test(10,20)
#print (t)  # this returns __str__ function

print ([t])  #this returns __repr__ function

