    # -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 09:13:13 2018

@author: lili
"""

class Myclass:
    
    #Hidden member of my class
    __hiddenVariable = 0
    
    #A member method that changes hiddenVariable
    def add(self, increment):
        self.__hiddenVariable +=increment
        print ("value i: ", self.__hiddenVariable)
        
        
obj = Myclass()
obj.add(100)

print (obj.__hiddenVariable)

 