# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 08:01:53 2018

@author: lili
"""

# A python program to demonstrate inheritance
class Father:
    #def __init__(self):
        print ("I am the father class")
        
        def father_func():
            print ("an internal function of fatherclass")

class Mother():
    # __init__(self):
        print ("i am the mother class")

class Child( Father,Mother ):
    print ("i am the child class")
    
        #print ("i am the child class")
    
    

family = Child()
print(family.father_func)        